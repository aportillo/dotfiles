-- mini.deps install

local path_package = vim.fn.stdpath("data") .. "/site/"
local mini_path = path_package .. "pack/deps/start/mini.nvim"
if not vim.loop.fs_stat(mini_path) then
    vim.cmd('echo "Installing `mini.nvim`" | redraw')
    local clone_cmd = {
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/echasnovski/mini.nvim",
        mini_path,
    }
    vim.fn.system(clone_cmd)
    vim.cmd("packadd mini.nvim | helptags ALL")
    vim.cmd('echo "Installed `mini.nvim`" | redraw')
end

-- mini.deps setup
require("mini.deps").setup({ path = { package = path_package } })
local add, now, later = MiniDeps.add, MiniDeps.now, MiniDeps.later

-- ui
now(function() -- theme
    add({ source = "catppuccin/nvim", name = "catppuccin" })
    local catppuccin = require("catppuccin")
    catppuccin.setup({
        color_overrides = {
            mocha = {
                mantle = "#1e1e2e", -- makes status bar background match terminal background
            },
        },
    })
    vim.cmd.colorscheme("catppuccin-mocha")
end)
now(function() -- start screen
    local starter = require("mini.starter")
    starter.setup({
        autoopen = true,
        items = {
            starter.sections.recent_files(5, false, false),
        },
        header = [[]],
        footer = [[]],
    })
end)

-- options
now(function()
    require("mini.basics").setup({ -- common configuration presets
        options = { extra_ui = true, },
        autocommands = { relnum_in_visual_mode = true, }
    })
    vim.keymap.del("n", "gO")
    vim.keymap.del("n", "go")
    vim.keymap.set('n', '<Leader>O', 'v:lua.MiniBasics.put_empty_line(v:true)',
        { expr = true, desc = 'Put empty line above' })
    vim.keymap.set('n', '<Leader>o', 'v:lua.MiniBasics.put_empty_line(v:false)',
        { expr = true, desc = 'Put empty line below' })
end)
now(function()                     -- custom options
    vim.g.mapleader = " "
    vim.o.timeout = true           -- set timeout for triggering keymap
    vim.o.timeoutlen = 2000
    vim.opt.list = true            -- displaying whitespace characters
    vim.opt.listchars = { tab = "  ", trail = "·", nbsp = "␣" }
    vim.opt.scrolloff = 3          -- minimal number of screen lines to keep above and below the cursor
    vim.opt.nrformats = ""         -- use decimals instead of octals when using <C-a>
    -- indentation, filetype specific set in after folder
    vim.opt.smartindent = true     -- nvim attempts to add tabs on new lines
    vim.g.zig_fmt_parse_errors = 0 -- zig opens errors in quickfix list otherwise
    vim.filetype.add({ extension = { templ = "templ" } })
    vim.opt.wrap = true
end)

-- lsp
now(function()
    add({
        source = "neovim/nvim-lspconfig",
        depends = { "williamboman/mason.nvim", "williamboman/mason-lspconfig.nvim", "saghen/blink.cmp" },
    })
    local capabilities = require('blink.cmp').get_lsp_capabilities()
    -- https://github.com/williamboman/mason-lspconfig.nvim#available-lsp-servers
    local servers = {
        tinymist = {}, -- typst
        pylsp = {
            capabilities = capabilities,
            settings = {
                pylsp = {
                    plugins = {
                        -- formatter options
                        black = { enabled = false },
                        autopep8 = { enabled = false },
                        yapf = { enabled = false },
                        -- linter options
                        pylint = { enabled = false },
                        pyflakes = { enabled = false },
                        pycodestyle = { enabled = false },
                        mccabe = { enabled = false },
                        -- type checker
                        pylsp_mypy = { enabled = false },
                        -- auto-completion options
                        jedi_completion = { fuzzy = true },
                        -- import sorting
                        pyls_isort = { enabled = false },
                    },
                },
            },
        },
        ruff = {},
        gopls = {
            filetypes = { "go", "gomod", "gowork" },
            setting = {
                gopls = {
                    completeUnimported = true,
                    usePlaceholder = true,
                    analyses = {
                        unusedparams = true,
                        shadow = true,
                    },
                },
            },
        },
        lua_ls = {
            settings = {
                Lua = {
                    diagnostics = {
                        globals = { "vim", "MiniDeps" },
                    },
                },
            },
        },
        templ = {},
        html = {
            filetypes = { "html", "templ" },
        },
        htmx = {
            filetypes = { "html", "templ" }
        },
        tailwindcss = {
            filetypes = { "templ" },
            settings = {
                tailwindCSS = {
                    includeLanguages = { templ = "html" }
                }
            }
        },
        zls = {
            capabilities = capabilities,
            cmd = { "zls" },
            filetypes = { "zig", "zir" },
            single_file_support = true,
        },
    }

    require("mason").setup()
    require("mason-lspconfig").setup({
        ensure_installed = vim.tbl_keys(servers or {}),
        handlers = {
            function(server_name)
                local server = servers[server_name] or {}
                server.capabilities = vim.tbl_deep_extend("force", {}, capabilities, server.capabilities or {})
                require("lspconfig")[server_name].setup(server)
            end,
        },
    })
end)

-- treesitter
later(function()
    add({ -- language library, improved syntax highlighting
        source = 'nvim-treesitter/nvim-treesitter',
        hooks = { post_checkout = function() vim.cmd('TSUpdate') end },
    })
    require('nvim-treesitter.configs').setup({
        auto_install = true,
        -- https://github.com/nvim-treesitter/nvim-treesitter?tab=readme-ov-file#supported-languages
        ensure_installed = { "lua", "go", "python", "zig", "sql" },
        highlight = { enable = true },
    })
end)
later(function()
    add({ -- treesitter based navigation
        source = "nvim-treesitter/nvim-treesitter-textobjects",
        depends = { "nvim-treesitter/nvim-treesitter" },
    })
    require('nvim-treesitter.configs').setup({
        textobjects = {
            move = {
                enable = true,
                set_jumps = true,
                goto_next_start = {
                    ["]m"] = { query = "@call.outer", desc = "Call forward" },
                    ["]f"] = { query = "@function.outer", desc = "Function forward" },
                    ["]s"] = { query = "@class.outer", desc = "Class forward" },
                    ["]i"] = { query = "@conditional.outer", desc = "Conditional forward" },
                    ["]l"] = { query = "@loop.outer", desc = "Loop forward" },
                    ["]a"] = { query = "@parameter.inner", desc = "Argument forward" },
                    ["]="] = { query = "@assignment.outer", desc = "Assignment forward" },
                    ["]r"] = { query = "@return.outer", desc = "Return forward" },
                },
                goto_next_end = {
                    ["]M"] = { query = "@call.outer", desc = "Call last" },
                    ["]F"] = { query = "@function.outer", desc = "Function last" },
                    ["]S"] = { query = "@class.outer", desc = "Class last" },
                    ["]I"] = { query = "@conditional.outer", desc = "Conditional last" },
                    ["]L"] = { query = "@loop.outer", desc = "Loop last" },
                    ["]A"] = { query = "@parameter.outer", desc = "Argument last" },
                    ["]R"] = { query = "@return.outer", desc = "Return last" },
                },
                goto_previous_start = {
                    ["[m"] = { query = "@call.outer", desc = "Call backward" },
                    ["[f"] = { query = "@function.outer", desc = "Function backward" },
                    ["[s"] = { query = "@class.outer", desc = "Class backward" },
                    ["[i"] = { query = "@conditional.outer", desc = "Condition backward" },
                    ["[l"] = { query = "@loop.outer", desc = "Loop backward" },
                    ["[a"] = { query = "@parameter.inner", desc = "Argument backward" },
                    ["[="] = { query = "@assignment.outer", desc = "Assignment backward" },
                    ["[r"] = { query = "@return.outer", desc = "Return backward" },
                },
                goto_previous_end = {
                    ["[M"] = { query = "@call.outer", desc = "Call first" },
                    ["[F"] = { query = "@function.outer", desc = "Function first" },
                    ["[S"] = { query = "@class.outer", desc = "Class first" },
                    ["[I"] = { query = "@conditional.outer", desc = "Conditional first" },
                    ["[L"] = { query = "@loop.outer", desc = "Loop first" },
                    ["[A"] = { query = "@parameter.outer", desc = "Argument first" },
                    ["[R"] = { query = "@return.outer", desc = "Return first" },
                },
            },
            select = {
                enable = true,
                lookahead = true,
                keymaps = {
                    -- You can use the capture groups defined in textobjects.scm
                    ["a="] = { query = "@assignment.outer", desc = "Select outer part of an assignment" },
                    ["i="] = { query = "@assignment.inner", desc = "Select inner part of an assignment" },
                    ["l="] = { query = "@assignment.lhs", desc = "Select left hand side of an assignment" },
                    ["r="] = { query = "@assignment.rhs", desc = "Select right hand side of an assignment" },
                    ["aa"] = { query = "@parameter.outer", desc = "Select outer part of an argument" },
                    ["ia"] = { query = "@parameter.inner", desc = "Select inner part of a argument" },
                    ["ai"] = { query = "@conditional.outer", desc = "Select outer part of a conditional" },
                    ["ii"] = { query = "@conditional.inner", desc = "Select inner part of a conditional" },
                    ["al"] = { query = "@loop.outer", desc = "Select outer part of a loop" },
                    ["il"] = { query = "@loop.inner", desc = "Select inner part of a loop" },
                    ["am"] = { query = "@call.outer", desc = "Select outer part of a function call" },
                    ["im"] = { query = "@call.inner", desc = "Select inner part of a function call" },
                    ["af"] = { query = "@function.outer", desc = "Select outer part of a function" },
                    ["if"] = { query = "@function.inner", desc = "Select inner part of a function" },
                    ["ac"] = { query = "@class.outer", desc = "Select outer part of a class" },
                    ["ic"] = { query = "@class.inner", desc = "Select inner part of a class" },
                    ["a/"] = { query = "@comment.outer", desc = "Select outer part of a comment" },
                    ["i/"] = { query = "@comment.inner", desc = "Select inner part of a comment" },
                },
            },
            swap = {
                enable = true,
                swap_next = {
                    ["<leader>xa"] = { query = "@parameter.inner", desc = "Swap argument with next" },
                    ["<leader>xf"] = { query = "@function.outer", desc = "Swap function with next" },
                },
                swap_previous = {
                    ["<leader>xA"] = { query = "@parameter.inner", desc = "Swap argument with previous" },
                    ["<leader>xF"] = { query = "@function.outer", desc = "Swap function with previous" },
                },
            },
        },

    })
    -- make treesitter moves repeatable
    local ts_repeat_move = require("nvim-treesitter.textobjects.repeatable_move")
    vim.keymap.set({ "n", "x", "o" }, ";", ts_repeat_move.repeat_last_move)
    vim.keymap.set({ "n", "x", "o" }, ",", ts_repeat_move.repeat_last_move_opposite)
end)

-- plugins
later(function()
    add({
        source = "folke/snacks.nvim",
    })
    require("snacks").setup({
        git = { enabled = true },
        gitbrowse = { enabled = true },
        lazygit = { enabled = true },
        statuscolumn = { enabled = true }, -- shows marks in status
    })
end)
later(function() require("mini.ai").setup() end) -- better around/inside textobjects
later(function()                                 -- add/delete/replace/find/highlight surrounding pairs
    require("mini.surround").setup({
        mappings = {
            find = "", -- disable find, rely on mini.ai's move to edge of object, g[
            find_left = "",
        }
    })
end)
later(function() require("mini.pairs").setup() end)     -- autopairs
later(function() require("mini.operators").setup() end) -- evaluate, exchange, multiply & replace text
now(function()                                          -- icon provider, used by statusline
    local MiniIcons = require("mini.icons")
    MiniIcons.setup()
    MiniIcons.mock_nvim_web_devicons()
end)
now(function() require("mini.git").setup() end) -- git integration, used to track git in statusline
now(function()
    require("mini.diff").setup({
        mappings = {
            reset = 'gh',
            apply = '' -- disabled, apply from cli/lazygit
        }
    })
end)                                                    -- work with git diff hunks
now(function() require("mini.statusline").setup() end)
later(function() require("mini.bracketed").setup() end) -- navigate with `]` & `[`
later(function()                                        -- file system navigation and manipulation
    require("mini.files").setup({
        mappings = {
            up = "<Up>",
            down = "<Down>",
            go_in = "<Right>",
            go_out = "<Left>",
        }
    })
end)
later(function() -- show next keymap clues
    local clue = require("mini.clue")
    clue.setup({
        triggers = {
            { mode = "n", keys = "<Leader>" },
            { mode = "x", keys = "<Leader>" },
            { mode = "n", keys = "g" },
            { mode = "x", keys = "g" },
            { mode = "n", keys = "s" },
            { mode = "n", keys = "'" },
            { mode = "n", keys = "`" },
            { mode = "x", keys = "'" },
            { mode = "x", keys = "`" },
            { mode = "n", keys = '"' },
            { mode = "x", keys = '"' },
            { mode = "i", keys = "<C-r>" },
            { mode = "c", keys = "<C-r>" },
            { mode = "n", keys = "[" },
            { mode = "n", keys = "]" },
        },
        clues = {
            clue.gen_clues.g(),
            clue.gen_clues.registers(),
            clue.gen_clues.marks(),
        },
        window = {
            delay = 500,
        }
    })
end)
later(function() -- improved jumping to next/previous single character
    require("mini.jump").setup({
        mappings = {
            repeat_jump = '', -- fixes conflict with treesitter repeating
        },
        delay = {
            highlight = 10000000, -- disable highlighting
        }
    })
end)
later(function() -- jump via iterative label filtering
    require("mini.jump2d").setup({
        view = {
            dim = true,
            n_steps_ahead = 2,
        },
        mappings = {
            start_jumping = '', -- map set in keymaps section to allow jumping to word start only
        },
    })
end)
later(function() require("mini.extra").setup() end)  -- extra pickers for mini.pick
later(function() require("mini.visits").setup() end) -- recent file tracking for mini.pick
later(function() require("mini.pick").setup() end)   -- picker
later(function()                                     -- immediate file navigation
    add({ source = 'cbochs/grapple.nvim' })
    require("grapple").setup({
        scope = "git",
        icons = true,
        status = true,
    })
end)
later(function() -- split/join arguments
    require("mini.splitjoin").setup({
        join = {
            hooks_post = { require("mini.splitjoin").gen_hook.del_trailing_separator() },
        },
        split = {
            hooks_post = { require("mini.splitjoin").gen_hook.add_trailing_separator() },
        }
    })
end)
later(function() -- move selection/line in any direction
    require("mini.move").setup({
        mappings = {
            left = "<S-left>",
            right = "<S-right>",
            up = "<S-up>",
            down = "<S-down>",
            line_left = "<S-left>",
            line_right = "<S-right>",
            line_up = "<S-up>",
            line_down = "<S-down>",
        }
    })
end)
later(function() -- autoformatting
    add({
        source = "stevearc/conform.nvim",
    })
    require("conform").setup({
        notify_on_error = true,
        format_on_save = {
            timeout_ms = 500,
            lsp_format = "fallback",
        },
        formatters_by_ft = {
            lua = { "stylua" },
            go = { "goimports", "gofmt" },
        },
    })
end)
later(function() -- autocompletion and snippets
    add({
        source = "saghen/blink.cmp",
        depends = { "rafamadriz/friendly-snippets" },
        checkout = "v0.8.2",
    })
    -- TODO: integrate with mini.snippets
    require("blink.cmp").setup({
        completion = {
            documentation = {
                auto_show = true,
                auto_show_delay_ms = 200,
            },
            ghost_text = {
                enabled = true
            }
        },
        signature = {
            enabled = true,
        },
    })
end)
later(function() -- testing mini.snippets
    local gen_loader = require("mini.snippets").gen_loader
    require("mini.snippets").setup({
        snippets = {
            gen_loader.from_lang(),
        }
    })
end)

-- mappings
vim.keymap.set({ "n", "x" }, "s", "<nop>") -- remove `s` for mini.surround
vim.keymap.set("n", "<Cr>", ":lua MiniJump2d.start(MiniJump2d.builtin_opts.word_start)<Cr>", { desc = "Jump" })
-- system clipboard
vim.keymap.set("v", "<Leader>y", '"+y', { noremap = true, desc = "Yank to system" })
vim.keymap.set("n", "<Leader>Y", '"+yg_', { noremap = true, desc = "Yank to end of line to system" })
vim.keymap.set("n", "<Leader>y", '"+y', { noremap = true, desc = "Yank to system" })
vim.keymap.set("n", "<Leader>yy", '"+yy', { noremap = true, desc = "Yank line to system" })
vim.keymap.set("n", "<Leader>p", '"+p', { noremap = true, desc = "Paste from system" })
vim.keymap.set("n", "<Leader>P", '"+P', { noremap = true, desc = "Paste above from system" })
vim.keymap.set("v", "<Leader>p", '"+p', { noremap = true, desc = "Paste from system" })
vim.keymap.set("v", "<Leader>P", '"+P', { noremap = true, desc = "Paste above from system" })
-- leader
vim.keymap.set("n", "<Leader>'", ":Pick resume<Cr>", { noremap = true, desc = "Last picker" })
vim.keymap.set("n", "<Leader>/", ":Pick grep_live<Cr>", { noremap = true, desc = "Fuzzy" })
vim.keymap.set("n", "<Leader>a", vim.lsp.buf.code_action, { desc = "Code action" })
vim.keymap.set("n", "<Leader>b", ":Pick buffers<Cr>", { desc = "Buffers" })
vim.keymap.set("n", "<Leader>d", ":Pick diagnostic<Cr>", { desc = "Diagnostics" })
vim.keymap.set("n", "<Leader>e", ":Grapple select index=2<Cr>", { desc = "Tag 2" })
vim.keymap.set("n", "<Leader>f", ":Pick files<Cr>", { desc = "Files" })
vim.keymap.set("n", "<Leader>F", ":lua MiniFiles.open()<Cr>", { desc = "File tree" })
vim.keymap.set("n", "<Leader>gb", ":lua Snacks.git.open()<Cr>", { desc = "Git blame" })
vim.keymap.set("n", "<Leader>gd", ":lua MiniDiff.toggle_overlay()<Cr>", { desc = "Diff overlay" })
vim.keymap.set("n", "<Leader>gg", ":lua Snacks.lazygit.open()<Cr>", { desc = "Lazygit" })
vim.keymap.set("n", "<Leader>gl", ":lua Snacks.gitbrowse.open()<Cr>", { desc = "Git link" })
vim.keymap.set("n", "<Leader>h", vim.diagnostic.open_float, { desc = "Diagnostic" })
vim.keymap.set("n", "<Leader>i", ":Grapple select index=3<Cr>", { desc = "Tag 3" })
vim.keymap.set("n", "<Leader>k", vim.lsp.buf.hover, { desc = "Documentation" })
vim.keymap.set("n", "<Leader>m", ":Grapple toggle_tags<Cr>", { desc = "Tags" })
vim.keymap.set("n", "<Leader>n", ":Grapple select index=1<Cr>", { desc = "Tag 1" })
-- vim.keymap.set("n", "<Leader>o", ":Pick visit_paths<Cr>", { desc = "Recent files" })
vim.keymap.set("n", "<Leader>r", vim.lsp.buf.rename, { desc = "Rename symbol" })
vim.keymap.set("n", "<Leader>s", ":lua MiniExtra.pickers.lsp({scope = 'document_symbol'})<Cr>", { desc = "Symbols" })
vim.keymap.set("n", "<Leader>t", ":Grapple toggle<Cr>", { desc = "Tag file" })
vim.keymap.set("n", "<Leader>T", ":lua MiniPick.builtin.grep({tool = 'rg', pattern = 'todo:'})<Cr>", { desc = "Todos" })
-- terminal like navigation in insert mode
vim.keymap.set("i", "<C-a>", "<Esc>I", { desc = "Start" })
vim.keymap.set("i", "<C-e>", "<Esc>A", { desc = "End" })
vim.keymap.set("i", "<M-f>", "<Esc><Space>wi", { desc = "Forward word" })
vim.keymap.set("i", "<M-b>", "<Esc><Space>bi", { desc = "Back word" })
vim.keymap.set("i", "<M-d>", "<Esc>cw", { desc = "Delete word" })
-- go to
vim.keymap.set("n", "ga", ":b#<CR>", { desc = "Last buffer" })
vim.keymap.set("n", "gd", vim.lsp.buf.definition, { desc = "Definition" })
vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { desc = "Declaration" })
vim.keymap.set("n", "gy", vim.lsp.buf.type_definition, { desc = "Type definition" })
vim.keymap.set("n", "gf", ":lua MiniExtra.pickers.lsp({scope='references'})<Cr>", { desc = "References" })

-- autocommands
-- clear command line after command completed
vim.api.nvim_create_autocmd("CmdlineLeave", {
    group = vim.api.nvim_create_augroup("clear-cmd", { clear = true }),
    callback = function()
        vim.fn.timer_start(2000, function()
            print(" ")
        end)
    end,
})

-- Templ go to definition fix
local function go_goto_def()
    local old = vim.lsp.buf.definition
    local opts = {
        on_list = function(options)
            if options == nil or options.items == nil or #options.items == 0 then
                return
            end
            local targetFile = options.items[1].filename
            local prefix = string.match(targetFile, "(.-)_templ%.go$")
            if prefix then
                local function_name = vim.fn.expand("<cword>")
                options.items[1].filename = prefix .. ".templ"
                vim.fn.setqflist({}, " ", options)
                vim.api.nvim_command("cfirst")
                vim.api.nvim_command("silent! /templ " .. function_name)
            else
                old()
            end
        end,
    }
    vim.lsp.buf.definition = function(o)
        o = o or {}
        o = vim.tbl_extend("keep", o, opts)
        old(o)
    end
end

vim.api.nvim_create_autocmd("LspAttach", {
    group = vim.api.nvim_create_augroup("UserLspConfig", {}),
    callback = function(event)
        local bufopts = { noremap = true, silent = true, buffer = event.buf }
        if vim.bo.filetype == "go" then
            go_goto_def()
        end
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
    end
})
