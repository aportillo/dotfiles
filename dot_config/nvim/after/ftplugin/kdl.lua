vim.opt.tabstop = 4    -- the size of hard tabs, default value
vim.opt.shiftwidth = 4 -- number of spaces indented when using =, > and <
