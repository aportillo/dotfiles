vim.opt.tabstop = 4      -- the size of hard tabs, default value
vim.opt.expandtab = true -- use spaces instead of tab character
vim.opt.shiftwidth = 4   -- number of spaces indented when using =, > and <
vim.opt.softtabstop = 4  -- number of spaces inserted instead of a tab character
