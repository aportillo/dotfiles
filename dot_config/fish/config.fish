if status is-interactive
    # Commands to run in interactive sessions can go here
end

# remove fish greeting
set fish_greeting

# variables
set -x EDITOR nvim

# below are tool specific configurations
# ===
# zig
fish_add_path $HOME/bin/zig

# fzf
fzf --fish | source

# zellij
# load completions file
source $HOME/.config/fish/completions/zellij.fish

# ripgrep
set -g -x RIPGREP_CONFIG_PATH $HOME/.config/ripgrep/ripgreprc

# homebrew
fish_add_path /opt/homebrew/bin

# cargo
fish_add_path $HOME/.cargo/bin
set -g -x CARGO_TARGET_DIR $HOME/cargo

zoxide init fish | source

# path
fish_add_path /usr/local/opt/llvm/bin

# golang
set -x GOPATH (go env GOPATH)
set -x PATH $PATH (go env GOPATH)/bin

# remove fzf theming, figure out how to do this permanently
set -e FZF_DEFAULT_OPTS
# search hidden files/directories when using ctrl-alt-f but
# ignore git 
set fzf_fd_opts --hidden --exclude=.git

# tide prompt colours
set tide_git_color_branch f5c2e7
set tide_character_color a6e3a1
set tide_character_color_failure f38ba8
set tide_pwd_color_anchors 89b4fa
set tide_pwd_color_dirs 89b4fa
set tide_right_prompt_items virtual_env

# aliases
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias ls='eza --group-directories-first --icons'
alias lss='eza -l --no-permissions --no-user --icons --no-filesize --time-style relative -G --group-directories-first -s modified'
alias lst='eza -TL2'
alias du='du -h'

# ripgrep config file location
set -g -x RIPGREP_CONFIG_PATH $HOME/.config/ripgrep/ripgreprc

# =============================================================================
#
# Utility functions for zoxide.
#

# pwd based on the value of _ZO_RESOLVE_SYMLINKS.
function __zoxide_pwd
    builtin pwd -L
end

# A copy of fish's internal cd function. This makes it possible to use
# `alias cd=z` without causing an infinite loop.
if ! builtin functions --query __zoxide_cd_internal
    if builtin functions --query cd
        builtin functions --copy cd __zoxide_cd_internal
    else
        alias __zoxide_cd_internal='builtin cd'
    end
end

# cd + custom logic based on the value of _ZO_ECHO.
function __zoxide_cd
    __zoxide_cd_internal $argv
end

# =============================================================================
#
# Hook configuration for zoxide.
#

# Initialize hook to add new entries to the database.
function __zoxide_hook --on-variable PWD
    test -z "$fish_private_mode"
    and command zoxide add -- (__zoxide_pwd)
end

# =============================================================================
#
# When using zoxide with --no-cmd, alias these internal functions as desired.
#

if test -z $__zoxide_z_prefix
    set __zoxide_z_prefix 'z!'
end
set __zoxide_z_prefix_regex ^(string escape --style=regex $__zoxide_z_prefix)

# Jump to a directory using only keywords.
function __zoxide_z
    set -l argc (count $argv)
    if test $argc -eq 0
        __zoxide_cd $HOME
    else if test "$argv" = -
        __zoxide_cd -
    else if test $argc -eq 1 -a -d $argv[1]
        __zoxide_cd $argv[1]
    else if set -l result (string replace --regex $__zoxide_z_prefix_regex '' $argv[-1]); and test -n $result
        __zoxide_cd $result
    else
        set -l result (command zoxide query --exclude (__zoxide_pwd) -- $argv)
        and __zoxide_cd $result
    end
end

# Completions.
function __zoxide_z_complete
    set -l tokens (commandline --current-process --tokenize)
    set -l curr_tokens (commandline --cut-at-cursor --current-process --tokenize)

    if test (count $tokens) -le 2 -a (count $curr_tokens) -eq 1
        # If there are < 2 arguments, use `cd` completions.
        complete --do-complete "'' "(commandline --cut-at-cursor --current-token) | string match --regex '.*/$'
    else if test (count $tokens) -eq (count $curr_tokens); and ! string match --quiet --regex $__zoxide_z_prefix_regex. $tokens[-1]
        # If the last argument is empty and the one before doesn't start with
        # $__zoxide_z_prefix, use interactive selection.
        set -l query $tokens[2..-1]
        set -l result (zoxide query --exclude (__zoxide_pwd) --interactive -- $query)
        and echo $__zoxide_z_prefix$result
        commandline --function repaint
    end
end
complete --command __zoxide_z --no-files --arguments '(__zoxide_z_complete)'

# Jump to a directory using interactive search.
function __zoxide_zi
    set -l result (command zoxide query --interactive -- $argv)
    and __zoxide_cd $result
end

# =============================================================================
#
# Commands for zoxide. Disable these using --no-cmd.
#

abbr --erase cd &>/dev/null
alias cd=__zoxide_z

abbr --erase cdi &>/dev/null
alias cdi=__zoxide_zi

# yazi: enable cd on quit
function y
	set tmp (mktemp -t "yazi-cwd.XXXXXX")
	yazi $argv --cwd-file="$tmp"
	if set cwd (command cat -- "$tmp"); and [ -n "$cwd" ]; and [ "$cwd" != "$PWD" ]
		builtin cd -- "$cwd"
	end
	rm -f -- "$tmp"
end
