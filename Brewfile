tap "armmbed/formulae"
tap "homebrew/bundle"
tap "homebrew/cask-fonts"
tap "homebrew/services"
tap "osx-cross/arm"
tap "osx-cross/avr"
tap "qmk/qmk"
# Needed for qmk
brew "armmbed/formulae/arm-none-eabi-gcc"
# Clone of cat(1) with syntax highlighting and Git integration
brew "bat"
# Manage your dotfiles across multiple diverse machines, securely
brew "chezmoi"
# GNU File, Shell, and Text utilities
brew "coreutils"
# Get a file from an HTTP, HTTPS or FTP server
brew "curl"
# View disk space usage and delete unwanted data, fast
brew "dua-cli"
# Modern, maintained replacement for ls
brew "eza"
# Simple, fast and user-friendly alternative to find
brew "fd"
# User-friendly command-line shell for UNIX-like operating systems
brew "fish"
# Command-line fuzzy finder written in Go
brew "fzf"
# Distributed revision control system
brew "git"
# Open source programming language to build simple/reliable/efficient software
brew "go"
# Fast linters runner for Go
brew "golangci-lint"
# Post-modern modal text editor
brew "helix"
# Interpreted, interactive, object-oriented programming language
brew "python@3.13"
# User-friendly cURL replacement (command-line HTTP client)
brew "httpie"
# Lightweight and flexible command-line JSON processor
brew "jq"
# Handy way to save and run project-specific commands
brew "just"
# Simple terminal UI for git commands
brew "lazygit"
# Language Server Protocol for Markdown
brew "marksman"
# Ambitious Vim-fork focused on extensibility and agility
brew "neovim"
# Object-relational database system
brew "postgresql@16", restart_service: :changed, link: true
# Quantum Mechanical Keyboard (QMK) Firmware
brew "qmk/qmk/qmk"
# Rsync for cloud storage
brew "rclone"
# Search tool like grep and The Silver Searcher
brew "ripgrep"
# Extremely fast Python linter, written in Rust
brew "ruff"
# Experimental Rust compiler front-end for IDEs
brew "rust-analyzer"
# Intuitive find & replace CLI
brew "sd"
# 7-Zip is a file archiver with a high compression ratio
brew "sevenzip"
# Open source continuous file synchronization application
brew "syncthing", restart_service: :changed
# LSP for TailwindCSS
brew "tailwindcss-language-server"
# CLI tool that moves files or folder to the trash
brew "trash"
# Markup-based typesetting system
brew "typst"
# Extremely fast Python package installer and resolver, written in Rust
brew "uv"
# Execute commands when watched files change
brew "watchexec"
# Blazing fast terminal file manager written in Rust, based on async I/O
brew "yazi"
# Pluggable terminal workspace, with terminal multiplexer as the base feature
brew "zellij"
# Shell extension to navigate your filesystem faster
brew "zoxide"

# GPU-accelerated terminal emulator
cask "alacritty"
# Automatic tiling window manager similar to xmonad
cask "amethyst"
# Transfer files from and to an Android smartphone
cask "android-file-transfer"
# Memory training application
cask "anki"
# Universal database tool and SQL client
cask "dbeaver-community"
# Web browser
cask "firefox"
cask "font-fira-code-nerd-font"
# Keyboard shortcuts for every button on your screen
cask "homerow"
# Free and open-source media player
cask "iina"
# Automation software
cask "keyboard-maestro"
# Knowledge base that works on top of a local folder of plain text Markdown files
cask "obsidian"
# PDF reader, editor and annotator
cask "pdf-expert"
# Toolbox companion for QMK Firmware
cask "qmk-toolbox"
# Catalogue of hi-res music for streaming and download
cask "qobuz"
# Modern programming language in the Lisp/Scheme family
cask "racket"
# Control your tools with a few keystrokes
cask "raycast"
# Sound and audio controller
cask "soundsource"
# Customise mouse buttons, wheels and cursor speed
cask "steermouse"
# Search and click text anywhere on screen
cask "superkey"
