# dotfiles

Using [chezmoi](https://www.chezmoi.io/) to manage my dotfiles.

## tools managed
- alacritty
- asdf
- fish
- git
- helix
- nvim
- ripgrep
- zellij
